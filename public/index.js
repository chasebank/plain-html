new Vue({
  el: '#app',
  
  data: () => ({
    points: [
            { i: true, t: "My name is Chase and I'm <em class='veryimportant'><strong>one of</strong></em> Emmanuel's best friends" },
      
      { t: "<em><strong>one of</strong></em>" },
      
      { t: "<em>Almost</em> 20 years" },
      
      { t: "I ask myself how I got to be so lucky" },
      
      { t: "But I know why Emmanuel has so many friends" },
      
      { t: "Obviously he's good hearted & loyal" },
      
      { t: "Most of all, he makes us laugh" },
            
      { t: "Tells jokes & stories. <em>He insults us</em>" },
      
      { i: true, t: "His humor is like a SUPERPOWER" },
      
      { t: "It's disarming becuase he makes us smile - <em>most valuable gift</em>" },
      
      { t: "It's no wonder why Angela <em>chose him</em>" },
      
      { t: "Now, compared to Emmanuel who's usually a loud & crazy fool" },
      
      { t: "The first time I met Angela, <em>she</em> was the opposite" },
      
      { t: "Believe it or not she was quiet and shy" },
      
      { i: true, t: "Don't get me <em>wrong</em>, that wore off quick" },
      
      { t: "Must have been intimidating" },
      
      { t: "<em>Quickly</em> got to know her better"},
      
      { t: "Looking back it's hard to imagine her as  quiet and shy" },
      
      { i: true, t: "Now I know better" },
      
      { t: "Just like Emmanuels loud, Angela gets loud..." },
      
      { t: "<em>and Angry...</em>" },
      
      { i: true, t: "As fierce as her Anger is, she's fiercely loving and loyal" },
      
      { i: true, t: "Most of all to both Cayden and The Baby" },
      
      { t: "She has a strength like a <em>forcefield</em>" },
      
      { t: "You don't want to get caught on the outside" },
      
      { t: "But if she let's you in, she's got your back, and will fight for you, and inspire you to fight for yourself." },
      
      { t: "No surprise to me why Emmanuel <em>chose her</em>" },
            
      { t: "Now that I know Angela" },
      
      { i: true, t: "She's become a part of my group" },
      
      { i: true, t: "When I think of my best friends, brothers, My Sister" },
      
      { t: "Toast to my Brother and Sister" }
    ]
  }),
  
  methods: {
    done(e) {
      e.currentTarget.classList.toggle('done')
    }
  },
  
  created() {
    if ('addEventListener' in document) {
      document.addEventListener('DOMContentLoaded', function() {
        FastClick.attach(document.body);
      }, false);
    }
  }
});